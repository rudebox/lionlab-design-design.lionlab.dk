<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!--[if lt IE 8]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<header class="header" id="header">
  <div class="wrap hpad flex flex--center flex--justify">

    <a class="header__logo"
       href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>">
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span>
      <img class="nav-toggle__img" src="<?php echo get_template_directory_uri(); ?>/assets/img/burger.png" alt="burger-menu">
    </div>

    <nav class="nav" role="navigation">
      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>
      </div>
    </nav>

    <?php if (is_single() ) : ?>
      <a class="nav__close" href="#"><i class="fas fa-times"></i> Luk denne bjælke</a>
    <?php endif; ?>

  </div>
</header>
