<?php get_template_part('parts/header'); the_post(); ?> 

<main style="background-color:<?php echo the_field('bg'); ?>">

  <section class="center padding--both no-responsive">
    <?php if (have_rows('design_files') ) : 
      while (have_rows('design_files') ) : the_row(); 
      $img =  get_sub_field('img');

      //trim url for easier string replacement
      $trim_url = parse_url($img['url'], PHP_URL_PATH);

      //replace and strip string from videolink variable down to video ID for thumbnail use
      $img = str_replace('-scaled', '', $trim_url);
    ?>

      <img src="<?php echo $img; ?>">

    <?php endwhile; endif; ?>

    <?php $video = get_field('video'); ?>

   	<?php if ($video) : ?>
   	  <div class="video-container">
	      <video id="video" class="video" autoplay preload="auto" loop muted="muted" volume="0" poster="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQ">
				<source src="<?php echo $video; ?>" type="video/mp4" codecs="avc1, mp4a">
				Din browser understøtter ikke HTML5 video. Opgrader venligst din browser.
		  </video>
	  </div>	
	<?php endif; ?>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>