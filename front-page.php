<?php get_template_part('parts/header'); ?>

<main>

  <?php 

  	// Query Arguments
	$args = array(
		'posts_per_page' => 1,
		'order' => 'DESC',
	);

	// The Query
	$query = new WP_Query( $args );

	// The Loop
	if ($query->have_posts() ) : 

	while ($query->have_posts() ) : $query->the_post(); ?>

	<?php $name = get_field('client_name'); ?>

	<section class="purple--bg padding--both">
		<div class="wrap hpad clearfix">
			<div class="row">

				<div class="info__intro tencol">
					<strong>Hej <?php echo $name; ?></strong> <br>
					Velkommen til din designpræsentation. Inden vi går til designet, har vi skrevet en kort informationstekst for at undgå alt for meget forvirring. Vi håber du bliver ligeså vild med designet som vi er.
				</div>

			</div>
		</div>
	</section>

	<section class="info padding--both">
		<div class="wrap hpad clearfix">

			<h3 class="info__heading">Skærmbredde</h3>

			<div class="info__container clearfix row flex flex--wrap">	
				<div class="fourcol info__screensize">
					<h4 class="info__title">Din skærmbredde</h4>
					<p class="info__screensize--browserwidth"></p>
				</div>

				<div class="fourcol info__screensize">
					<h4 class="info__title">Designets bredde</h4>
					<p class="info__screensize--designwidth">1600 px</p>
				</div>

				<div class="fourcol info__screensize comparison">
					<h4 class="info__title">Skærmbredde resultat</h4>
					<p class="info__screensize--comparisonwidth"></p>
				</div>
			</div>

			<p>Designet er kun et billedet indtil videre og vil derfor ikke være klikbart eller responsivt. Når designet bliver programmeret vil det selvfølgelig tilpasse sig alle formater, såsom mobiltelefon, tablet og computer</p>

			<div class="center">
				<a class="btn btn--gradient" href="<?php the_permalink(); ?>">Se design her</a>
			</div>
		</div>
	</section>	


	<?php wp_reset_postdata(); ?>

	<?php endwhile; endif; ?>

</main>

<?php get_template_part('parts/footer'); ?>
