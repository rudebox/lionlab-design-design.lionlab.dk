jQuery(document).ready(function($) {

    //Browser - design width comparison

    var width = $(window).width();
    var screensize = "";
    var px = " px";
    var succes = "Du vil se designet i fuld format";
    var error = "OBS Din skærmstørrelse stemmer ikke overens med designet bredde. Scroll til siden for at se resten af designet der er udenfor skærmen.";

    $('.info__screensize--browserwidth').prepend(width + px);

    if (width > 1600)  {
      $('.info__screensize--comparisonwidth').html(succes);
      $('.comparison').addClass('succes');
    }

    if (width < 1600) {
      $('.info__screensize--comparisonwidth').html(error); 
      $('.comparison').addClass('error');
    }
    
    //print screensize
    $(window).resize(function() { 
      var width = $(window).width();
      var screensize = "";
      var px = " px";
      
      screensize = $(window).width();
      $('.info__screensize--browserwidth').html(screensize + px);

      if (width > 1600)  {
        $('.info__screensize--comparisonwidth').html(succes);
        $('.comparison').addClass('succes');
        $('.comparison').removeClass('error');
      }

      if (width < 1600) {
       $('.info__screensize--comparisonwidth').html(error); 
       $('.comparison').addClass('error');
       $('.comparison').removeClass('succes');
      }
  
    });


    //Hide header

    $('.nav__close').on('click', function() {
        $('header').slideUp();
    });
});

